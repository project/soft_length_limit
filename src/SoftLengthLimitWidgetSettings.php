<?php

namespace Drupal\soft_length_limit;

/**
 * Allowed settings helper.
 */
class SoftLengthLimitWidgetSettings {

  /**
   * Returns the widget settings that can be used for a soft_length widget.
   *
   * @return array
   *   An array of settings and default values for each textfield type.
   */
  public static function allowedForPlugin(string $plugin_id): array {
    $settings = [
      'string_textfield' => [
        'max_limit' => TRUE,
        'minimum_limit' => TRUE,
        'style_select' => TRUE,
      ],
      'string_textarea' => [
        'max_limit' => TRUE,
        'minimum_limit' => TRUE,
        'style_select' => TRUE,
      ],
      'text_textfield' => [
        'max_limit' => TRUE,
        'minimum_limit' => TRUE,
        'style_select' => TRUE,
      ],
      'text_textarea' => [
        'max_limit' => TRUE,
        'minimum_limit' => TRUE,
        'style_select' => TRUE,
      ],
      'text_textarea_with_summary' => [
        'max_limit' => TRUE,
        'minimum_limit' => TRUE,
        'style_select' => TRUE,
      ],
    ];

    return $settings[$plugin_id] ?? [];
  }

}
