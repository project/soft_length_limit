<?php

/**
 * @file
 * Soft Length Limit Migrate module.
 */

use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Implements hook_migrate_prepare_row().
 */
function soft_length_limit_migrate_migrate_prepare_row(Row $row, MigrateSourceInterface $source, MigrationInterface $migration) {
  // Add migrate support to migrate D7 settings into D8 fields.
  $text_fields = [
    'text_textarea',
    'text_textfield',
    'text_textarea_with_summary',
    'text_long',
  ];

  if ($source->getPluginId() !== 'd7_field_instance_per_form_display' || !in_array($row->getSource()['type'], $text_fields)) {
    return;
  }

  $widget_settings = $row->getSourceProperty('widget')['settings'];

  if (!empty($widget_settings['soft_length_limit']) || !empty($widget_settings['soft_length_minimum'])) {
    $constants = $row->getSourceProperty('constants');
    $constants['third_party_settings']['soft_length_limit'] = [
      'max_limit' => $widget_settings['soft_length_limit'],
      'minimum_limit' => $widget_settings['soft_length_minimum'],
      'style_select' => $widget_settings['soft_length_style_select'],
    ];
    $row->setSourceProperty('constants', $constants);
  }
}
