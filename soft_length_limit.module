<?php

/**
 * @file
 * Soft Length Limit module.
 */

declare(strict_types = 1);

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\soft_length_limit\SoftLengthLimitWidgetSettings;

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function soft_length_limit_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state) {
  if (!$allowed_settings = SoftLengthLimitWidgetSettings::allowedForPlugin($plugin->getPluginId())) {
    return;
  }

  $element = [
    '#type' => 'fieldset',
    '#title' => 'Soft length limit',
  ];
  if ($allowed_settings['max_limit']) {
    $element['max_limit'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => t('Soft length limit'),
      '#default_value' => $plugin->getThirdPartySetting('soft_length_limit', 'max_limit'),
      '#description' => t('If any value is given here, a counter will appear next to this field, informing the user of the chosen number of allowed characters. If the number is exceeded, a warning will be shown.'),
      '#weight' => -3,
    ];
  }

  if ($allowed_settings['minimum_limit']) {
    $element['minimum_limit'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => t('Soft length minimum'),
      '#default_value' => $plugin->getThirdPartySetting('soft_length_limit', 'minimum_limit'),
      '#description' => t('If any value is given here, the minimum number recommended characters will be displayed as the editor enters text in this field.'),
      '#weight' => -2,
    ];
  }

  if ($allowed_settings['style_select']) {
    $element['style_select'] = [
      '#type' => 'checkbox',
      '#title' => t('Minimal styling'),
      '#default_value' => $plugin->getThirdPartySetting('soft_length_limit', 'style_select'),
      '#description' => t('Enable a minimal view of limit state, with icons.'),
      '#weight' => -1,
    ];
  }

  return $element;
}

/**
 * Implements hook_field_widget_settings_summary_alter().
 */
function soft_length_limit_field_widget_settings_summary_alter(&$summary, $context) {
  /* @var \Drupal\Core\Field\WidgetInterface $widget */
  $widget = $context['widget'];
  if (!$allowed_settings = SoftLengthLimitWidgetSettings::allowedForPlugin($widget->getPluginId())) {
    return;
  }

  $max_limit = $allowed_settings['max_limit']
    ? $widget->getThirdPartySetting('soft_length_limit', 'max_limit')
    : FALSE;
  $minimum_limit = $allowed_settings['minimum_limit']
    ? $widget->getThirdPartySetting('soft_length_limit', 'minimum_limit')
    : FALSE;
  $style_select = $allowed_settings['minimum_limit']
    ? $widget->getThirdPartySetting('soft_length_limit', 'style_select')
    : FALSE;

  if ($max_limit) {
    $summary[] = t('Maximum recommended length: @count', ['@count' => $max_limit]);
  }
  if ($minimum_limit) {
    $summary[] = t('Minimum recommended length: @count', ['@count' => $minimum_limit]);
  }
  if ($style_select) {
    $summary[] = t('Minimal styling: @style', ['@style' => $style_select ? 'Enabled' : 'Disabled']);
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function soft_length_limit_field_widget_single_element_form_alter(&$element, FormStateInterface $form_state, $context) {
  $third_party_settings = $context['widget']->getThirdPartySettings();

  if (empty($third_party_settings['soft_length_limit'])) {
    return;
  }

  $sll_config = $third_party_settings['soft_length_limit'];

  if (isset($sll_config['max_limit'])) {
    $element['value']['#attributes']['data-soft-length-limit'] = $sll_config['max_limit'];
    $element['value']['#attributes']['class'][] = 'soft-length-limit';
  }
  if (isset($sll_config['minimum_limit'])) {
    $element['value']['#attributes']['data-soft-length-minimum'] = $sll_config['minimum_limit'];
  }
  // Length style select.
  if (isset($sll_config['style_select']) && $sll_config['style_select']) {
    $element['value']['#attributes']['data-soft-length-style-select'] = (int) $sll_config['style_select'];
  }

  if (isset($element['#type']) && $element['#type'] === 'text_format') {
    $element['#attributes'] = array_merge_recursive($element['#attributes'] ?? [], $element['value']['#attributes'] ?? []);
  }

  $element['#attached']['library'][] = 'soft_length_limit/soft_length_limit';
}
